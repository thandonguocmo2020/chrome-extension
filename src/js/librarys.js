function addCss() {
  let css = {
    databaseTable: "//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css",
    botstrap: "//l.allcdn.org/remark/v4.1/bootstrap.min.css",
    extend: "//l.allcdn.org/remark/v4.1/bootstrap-extend.min.css",
    site: "//l.allcdn.org/remark/v4.1/site.min.css",
    color: "//l.allcdn.org/remark/v4.1/skins/green.min.css",
    fontawesome: "https://use.fontawesome.com/releases/v5.0.10/css/all.css"
  };

  for (let item in css) {
    var cssId = item; // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId)) {
      var head = document.getElementsByTagName("head")[0];
      var link = document.createElement("link");
      link.id = cssId;
      link.rel = "stylesheet";
      link.type = "text/css";
      link.href = css[item];
      link.media = "all";
      head.appendChild(link);
    }
  }
}

function setJquey() {
  var js = {
    jquery: "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js",
    Popper:
      "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.1/umd/popper.min.js",
    boostrap:
      "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js",
    vue: "https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js",
    "vue-router":
      "https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js",
    vuex: "https://cdnjs.cloudflare.com/ajax/libs/vuex/3.0.1/vuex.min.js",
    axios: "https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js",
    databaseTable: "//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"
  };

  for (let item in js) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", js[item]);
    document.head.appendChild(script);
  }
}

addCss();
setJquey();
